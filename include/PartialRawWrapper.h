#ifndef PARTIALRAWWRAPPER_H
#define PARTIALRAWWRAPPER_H
#include <iostream>
#include "RawWrapper.h"
#include <boost/serialization/vector.hpp>

class PartialRawWrapper : public RawWrapper
{
public:
    PartialRawWrapper();
    virtual ~PartialRawWrapper();
    int maxValue();
    RawWrapper * copy();
    void cropToActiveArea();
    void doWhiteBalance();
    static std::vector<PartialRawWrapper> split(RawWrapper& wrapper, int howMany);
    void getCopy(RawWrapper & wrapper, int width, int height, int startingx, int startingy);
    boost::array<boost::array<double,3>,3> getColorArray();
    ///boost archive
    template<class Archive>
    void save(Archive & ar, const unsigned int version) const
    {
        ar & height;
        ar & width;
        ar & extend;
        ar & bayerOffsets;
        ar & multipliers;
        ar & internalSettings;
        ar & blackLevels;
        ar & colorOffsets;
        ar & maxValueConst;
        ar & matrix;
        ar & startingx;
        ar & startingy;
        vector<unsigned short> temp;
        temp.insert(temp.begin(), extendedRawImage.get(), extendedRawImage.get()+getSize());
        ar & temp;
    }
    template <typename Archive>
    void load(Archive& ar, const unsigned int version)
    {
        ar & height;
        ar & width;
        ar & extend;
        ar & bayerOffsets;
        ar & multipliers;
        ar & internalSettings;
        ar & blackLevels;
        ar & colorOffsets;
        ar & maxValueConst;
        ar & matrix;
        ar & startingx;
        ar & startingy;
        vector<unsigned short> temp;
        ar & temp;
        std::shared_ptr<ushort> ptr(new ushort[temp.size()], array_deleter<ushort>());
        extendedRawImage=ptr;
        std::copy(temp.begin(), temp.end(), extendedRawImage.get());
    }
    BOOST_SERIALIZATION_SPLIT_MEMBER()
    void getCopy(RawWrapper & wrapper);
    int startingx=0;
    int startingy=0;
protected:
private:
    int maxValueConst;
    boost::array<boost::array<double,3>,3> matrix;
};
class Splitter
{
public:
    RawWrapper * wrapper;
    std::vector<PartialRawWrapper> parts;
    void splitter(int width, int height, int startingx, int startingy, int howMany);
};
#endif // PARTIALRAWWRAPPER_H
