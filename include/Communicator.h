#ifndef COMMUNICATOR_H
#define COMMUNICATOR_H
#include "PseudoUI.h"
#include "SerializationObjects.h"
#include "IdentifiedSerializationObjects.hpp"
#include "AsioCommunication.h"
#include <list>

using namespace std;
using namespace boost;
class Communicator
{
public:
    Communicator();
    ~Communicator();
    void sendFile(InputData data);
    void receive();
    void init();
    void resetConnection();
    PseudoUI * ui;
    string address;
    condition_variable newPart;
    mutex mut;
    list<OutputData> outputQueue;
private:
    AsioCommunication * comm=nullptr;
    unsigned int id;
    thread* connectionThread=nullptr;

};

#endif // COMMUNICATOR_H
