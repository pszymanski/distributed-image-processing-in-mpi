/***************************************************************
 * Name:      GUIProjectApp.h
 * Purpose:   Defines Application Class
 * Author:     ()
 * Created:   2014-06-30
 * Copyright:  ()
 * License:
 **************************************************************/

#ifndef GUIPROJECTAPP_H
#define GUIPROJECTAPP_H

#include <wx/app.h>

class GUIProjectApp : public wxApp
{
    public:
        virtual bool OnInit();
};

#endif // GUIPROJECTAPP_H
