#ifndef MPIWRAPPER_H
#define MPIWRAPPER_H
#include <boost/mpi/environment.hpp>
#include <boost/mpi/communicator.hpp>
#include <boost/mpi/collectives.hpp>
using namespace boost;
using namespace std;

class MPIWrapper
{
public:
    MPIWrapper();
    virtual ~MPIWrapper();
    int getRank();
    int getSize();
    template<typename T>
    void broadcast(T &value, int broadcasterId)
    {
        mpi::broadcast(world, value, broadcasterId);
    }
    template<typename T>
    void gather(T myValue, vector<T>&allValues, int gatherId)
    {
        mpi::gather(world, myValue, allValues,gatherId);
    }
    template<typename T>
    void gather(T myValue, int gatherId)
    {
        mpi::gather(world, myValue, gatherId);
    }
    template<typename T>
    void scatter(T & myValue, vector<T>&allValues, int scatterId)
    {
        mpi::scatter(world, allValues, myValue, scatterId);
    }
    template<typename T>
    void scatter(T & myValue, int scatterId)
    {
        mpi::scatter(world, myValue, scatterId);
    }
    template<typename T>
    void globalMaximum(T & input)
    {
        mpi::all_reduce(world,mpi::inplace(input), mpi::maximum<T>());
    }
protected:
private:
    mpi::environment env;
    mpi::communicator world;

};

#endif // MPIWRAPPER_H
