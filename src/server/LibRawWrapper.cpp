#include "LibRawWrapper.h"
#include <algorithm>
using namespace std;
LibRawWrapper::LibRawWrapper()
{
    //ctor
}

LibRawWrapper::~LibRawWrapper()
{
    //dtor
}
LibRawWrapper::LibRawWrapper(InputData& input)
{
    this->internalSettings=input;

}
void LibRawWrapper::free()
{
    iProcessor.recycle();
}
RawWrapper* LibRawWrapper::copy()
{
    LibRawWrapper * wrapper = new LibRawWrapper(*this);
    //wrapper->internalSettings=internalSettings;
    //wrapper->height=height;
    //wrapper->width=width;
    //wrapper->extend=extend;
    int size=(height+(extend<<1))*(width+(extend<<1));
    shared_ptr<ushort> ptr(new ushort[size], array_deleter<ushort>());
    extendedRawImage=ptr;
    std::copy(extendedRawImage.get(), extendedRawImage.get()+size, wrapper->extendedRawImage.get());
    wrapper->buildArray();
    return wrapper;
    //return new LibRawWrapper(*this);
}
int LibRawWrapper::open()
{
    free();
    int ret;
    if( (ret = iProcessor.open_buffer(internalSettings.buffer.data(),internalSettings.buffer.size())) != LIBRAW_SUCCESS)
    {
        logger.log((boost::format("Cannot read buffer: %s")%libraw_strerror(ret)).str());
        return ret;
    }
    if( (ret = iProcessor.unpack() ) != LIBRAW_SUCCESS)
    {
        logger.log((boost::format("Cannot unpack buffer: %s")%libraw_strerror(ret)).str());
        return ret;
    }
    decodeOffsets(0);
    if(internalSettings.verbose)
        logger.log("Image loaded");
    int black;
    if(internalSettings.customBlackLevel)
    {
        black = internalSettings.blackLevel;
        if(internalSettings.verbose)
            logger.log((boost::format("Using custom black level %d")%black).str());
    }
    else
    {
        black =iProcessor.imgdata.color.black;
        if(internalSettings.verbose)
            logger.log((boost::format("Using automatic black level %d")%black).str());
    }
    for(int j=0; j<4; j++)
        blackLevels[j]=iProcessor.imgdata.color.cblack[j]+black;
    return 0;

}
void LibRawWrapper::decodeOffsets(int i)
//@param index to start
{
    int value = iProcessor.COLOR(i,i);
    colorOffsets[value].x=0;
    colorOffsets[value].y=0;
    //cout<<value<<endl;
    bayerOffsets[0]=value;
    value = iProcessor.COLOR(i,i+1);
    colorOffsets[value].x=0;
    colorOffsets[value].y=1;
    //cout<<value<<endl;
    bayerOffsets[1]=value;
    value = iProcessor.COLOR(i+1,i);
    colorOffsets[value].x=1;
    colorOffsets[value].y=0;
    //cout<<value<<endl;
    bayerOffsets[2]=value;
    value = iProcessor.COLOR(i+1,i+1);
    colorOffsets[value].x=1;
    colorOffsets[value].y=1;
    //cout<<value<<endl;
    bayerOffsets[3]=value;
}
void LibRawWrapper::doWhiteBalance()
{
    auto color = iProcessor.imgdata.color;
    if(color.cam_mul[0]*color.cam_mul[1]*color.cam_mul[2]>0)
    {
        if(internalSettings.verbose)
            logger.log("using camera white balance");
        std::copy(iProcessor.imgdata.color.cam_mul, iProcessor.imgdata.color.cam_mul+4, multipliers.data());
        //multipliers=boost::array(iProcessor.imgdata.color.cam_mul);
    }
    else
    {
        if(internalSettings.verbose)
            logger.log("using libraw white balance");
        //multipliers=iProcessor.imgdata.color.pre_mul;
        std::copy(iProcessor.imgdata.color.cam_mul, iProcessor.imgdata.color.cam_mul+4, multipliers.data());
    }
    if(multipliers[3]==0.0)//for RGBG arrays
        multipliers[3]=multipliers[1]; //copy green to green
    float max = *min_element(multipliers.begin(), multipliers.end());
    for(int i=0; i<4; i++)
    {
        multipliers[i]/=max;
    }
    if(internalSettings.verbose)
    {
        logger.log((boost::format("Color multipliers %s")%iProcessor.imgdata.idata.cdesc).str());
        logger.log((boost::format("%d %d %d %d")%multipliers[0]%multipliers[1]%multipliers[2]%multipliers[3]).str());
    }
}
boost::array<boost::array<double,3>,3> LibRawWrapper::getColorArray()
{
    auto color = iProcessor.imgdata.color;
    boost::array<boost::array<double,3>,3> matrix;
//    double* matrixdata = new double[9];
//    double** matrix= new double*[3];
//    for(int j=0,l=0; j<3; j++)
//    {
//        matrix[j]=&matrixdata[l];
//        l+=3;
//    }
    for(int j=0; j<3; j++)
        for(int i=0; i<3; i++)
        {
            matrix[j][i]=color.rgb_cam[j][i];
        }
    return matrix;
}
int LibRawWrapper::maxValue()
{
    int ret;
    if(internalSettings.customSaturationLevel)
    {
        ret=internalSettings.saturationLevel;
        if(internalSettings.verbose)
            logger.log((boost::format("Using custom saturation level %d")%ret).str());
    }
    else
    {
        ret=iProcessor.imgdata.color.maximum;
        if(internalSettings.verbose)
            logger.log((boost::format("Using automatic saturation level %d")%ret).str());
    }
    return ret;
}
void LibRawWrapper::cropToActiveArea()
{
    ushort* rawImage;
    rawImage=iProcessor.imgdata.rawdata.raw_image;
    ushort * cropArea;
    setExtendValue();

    if(iProcessor.imgdata.sizes.raw_height!=iProcessor.imgdata.sizes.height
            ||iProcessor.imgdata.sizes.raw_width!=iProcessor.imgdata.sizes.width)
    {
        cropArea = new ushort[iProcessor.imgdata.sizes.height*iProcessor.imgdata.sizes.width];
        ushort * dest = cropArea;
        ushort * src = rawImage;
        src+=iProcessor.imgdata.sizes.raw_width*iProcessor.imgdata.sizes.top_margin;//skip top
        for(int j=0; j<iProcessor.imgdata.sizes.height; j++)
        {
            src+=iProcessor.imgdata.sizes.left_margin;//crop left margin
            std::copy(src, src+iProcessor.imgdata.sizes.width, dest);
            dest+=iProcessor.imgdata.sizes.width;
            src+=(iProcessor.imgdata.sizes.raw_width-iProcessor.imgdata.sizes.left_margin);//crop right margin
        }
        rawImage=cropArea;
        height=iProcessor.imgdata.sizes.height;
        width=iProcessor.imgdata.sizes.width;
        ushort * extendedRawImagePtr = extendBayerArea(rawImage,extend/2);
        shared_ptr<ushort> ptr(extendedRawImagePtr, array_deleter<ushort>());
        this->extendedRawImage.swap(ptr);
        delete[] cropArea;
    }
    else
    {
        height=iProcessor.imgdata.sizes.raw_height;
        width=iProcessor.imgdata.sizes.raw_width;
        ushort * extendedRawImagePtr=extendBayerArea(rawImage,extend/2);
        shared_ptr<ushort> ptr(extendedRawImagePtr, array_deleter<ushort>());
        this->extendedRawImage.swap(ptr);
    }

    buildArray();

    iProcessor.free_image(); //we don't need it right now
//formula to find color code on rawArray for given j - height, i - width
#define bayerFind bayerOffsets[(i&1)|(j&1)<<1]
    //deal with bad pixels
    ushort * extendedRawImagePtr=extendedRawImage.get();
    for(int j=0, l=((width+(extend<<1))*(extend)+extend); j <  height; j++)
    {
        for(int i=0; i < width; i++)
        {
            if(extendedRawImagePtr[l]==0)  //zero means defected pixel - interpolate bilinear
            {
                if(bayerFind&1)
                    //green value
                    extendedRawImagePtr[l]=(extendRawArray[j+1][i+1]+extendRawArray[j-1][i+1]+extendRawArray[j+1][i-1]+extendRawArray[j-1][i-1])>>2;
                else
                    extendedRawImagePtr[l]=(extendRawArray[j+2][i]+extendRawArray[j-2][i]+extendRawArray[j][i+2]+extendRawArray[j][i-2])>>2;

            }
            l++;
        }
        l+=extend<<1;
    }

}

