#ifndef GAMMALUT_H_INCLUDED
#define GAMMALUT_H_INCLUDED
#include <boost/preprocessor/repetition/enum.hpp>

///LUT generation at compile time
#define index4 65536
#ifndef GAMMAVALUE
#define GAMMAVALUE 2.2
#endif
#define GAMMA(z, n, ind) static_cast<unsigned short>(pow(double(256*ind+n)/index4, 1.0/GAMMAVALUE)*index4)
#define INNER(z, n, gamm) BOOST_PP_ENUM(256, GAMMA, n)

const static unsigned short LUT2[]={BOOST_PP_ENUM(256, INNER, 1)};

#endif // GAMMALUT_H_INCLUDED
