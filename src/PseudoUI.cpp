#include "PseudoUI.h"
#include "Communicator.h"
void PseudoUI::parseInput(int argc, char* argv[])
{
    const static struct option long_options[] =
    {
        {"verbose", no_argument,0, 1},
        {"help",   no_argument,0, 2},
        {"postfilter-mask",  required_argument, 0, 3},
        {"postfilter-method",  required_argument, 0, 4},
        {"prefilter-mask",  required_argument, 0, 5},
        {"prefilter-method",  required_argument, 0, 6},
        {"black-level",  required_argument, 0, 7},
        {"color-matrix",  required_argument, 0, 8},
        {"demosaic", required_argument, 0, 9},
        {"saturation-level",  required_argument, 0, 10},
        {"precision",  required_argument, 0, 11},
        {"address",  required_argument, 0, 11},
        {0, 0, 0, 0}
    };
    while(true)
    {
        int option_index=0;
        int c = getopt_long (argc, argv, "",long_options, &option_index);
        if (c == -1)
            break;
        switch(c)
        {
        case 1:
            inputSettings.verbose=true;
            break;
        case 2:
            printHelp();
            return;
            break;
        case 3:
            inputSettings.postfilterMask=atoi(optarg);
            break;
        case 4:
            inputSettings.postfilter=static_cast<postfilterType>(atoi(optarg));
            break;
        case 5:
            inputSettings.prefilterMask=atoi(optarg);
            break;
        case 6:
            inputSettings.prefilter=static_cast<prefilterType>(atoi(optarg));
            break;
        case 7:
            inputSettings.blackLevel=atoi(optarg);
            inputSettings.customBlackLevel=true;
            break;
        case 8:
            inputSettings.colorMatrix=static_cast<colorMatrixType>(atoi(optarg));
            break;
        case 9:
            inputSettings.demosaic=static_cast<demosaicMethod>(atoi(optarg));
            break;
        case 10:
            inputSettings.saturationLevel=atoi(optarg);
            inputSettings.customSaturationLevel=true;
            break;
        case 11:
            inputSettings.precision=atoi(optarg);
            break;
        case 12:
            address=string(optarg);
            break;
        }

    }
    if (optind < argc)
    {
        string inputString(argv[optind]);
        ifstream is (inputString, ifstream::binary);
        if (is)
        {
            std::vector<char>  buffer = vector<char>(std::istreambuf_iterator<char>(is),std::istreambuf_iterator<char>());
            inputSettings.buffer=buffer;
            Communicator con;
            con.ui=this;
            con.address=address;
            con.init();
            con.sendFile(inputSettings);
            PngReaderWriter writer;
            OutputData out;
            {
                unique_lock<mutex> lock(con.mut);
                while(con.outputQueue.size()==0)
                {
                    cout<<"locked1!"<<endl;
                    con.newPart.wait(lock);
                }
                out=con.outputQueue.back();
                con.outputQueue.pop_back();
            }
            WorkingImage<arrayType, 3> * image= new WorkingImage<arrayType, 3>(out.realWidth, out.realHeight);
            WorkingImage<unsigned char, 3> * imageChar= new WorkingImage<unsigned char, 3>(out.realWidth, out.realHeight);
            image->fill(0,0,0);
            imageChar->fill(0,0,0);
            unsigned int cumulativeSize=0;
            while(true)
            {
                cout<<out.realHeight<<" "<<out.realWidth<<endl;
                cout<<out.startingHeight<<" "<<out.startingWidth<<endl;
                pair<int, int> WH = writer.readPngFromMemory(*image, out);
                writer.readPngFromMemory(*imageChar, out);
                cumulativeSize+=WH.first*WH.second;
                if(cumulativeSize==out.realHeight*out.realWidth)
                    break;
                unique_lock<mutex> lock(con.mut);
                while(con.outputQueue.size()==0)
                {
                    cout<<"locked2!"<<endl;
                    con.newPart.wait(lock);
                }
                out =con.outputQueue.back();
                con.outputQueue.pop_back();
            }
            buffer = writer.writePngToMemory(*image, image->getPrecision());
            ofstream os ("test.png", ofstream::binary);
            if (os)
            {
                os.write(buffer.data(), buffer.size());
            }
            buffer = writer.writePngToMemory(*imageChar, imageChar->getPrecision());
            ofstream os2 ("testChar.png", ofstream::binary);
            if (os2)
            {
                os2.write(buffer.data(), buffer.size());
            }
            delete image;
            delete imageChar;
        }
    }
    else
        printHelp();
}
void PseudoUI::printHelp()
{
    cout<<"Usage: "<<endl;
    cout<<"RawCLI [options...] file"<<endl;
    cout<<"Demosaic options:"<<endl;
    cout<<"\t--demosaic=n\t select demosaic method, where n is number 0-3"<<endl;
    cout<<"\t\t\t 0 for Nearest Neighbor demosaic"<<endl;
    cout<<"\t\t\t 1 for Bilinear demosaic"<<endl;
    cout<<"\t\t\t 2 for Simple Gradients demosaic"<<endl;
    cout<<"\t\t\t 3 for VNG demosaic [default]"<<endl;

    cout<<"Color handling options:"<<endl;
    cout<<"\t--black-level=n"<<endl;
    cout<<"\t\tarbitary set black level value, where n is unsigned integer"<<endl;
    cout<<"\t--saturation-level=n"<<endl;
    cout<<"\t\tarbitary set saturation level value, where n is unsigned integer"<<endl;
    cout<<"\t--color-matrix=n"<<endl;
    cout<<"\t\tselect color matrix handling method, where n is number 0-1"<<endl;
    cout<<"\t\t\t 0 for none"<<endl;
    cout<<"\t\t\t 1 for dcraw default [default]"<<endl;
    cout<<"Filtering options:"<<endl;
    cout<<"\t--prefilter-method=n"<<endl;
    cout<<"\t\tselect prefilter method, where n is number 0-2"<<endl;
    cout<<"\t\t\t 0 for none [default]"<<endl;
    cout<<"\t\t\t 1 for statistics filtering"<<endl;
    cout<<"\t\t\t 2 for median filtering"<<endl;

    cout<<"\t--postfilter-method=n"<<endl;
    cout<<"\t\tselect postfilter method, where n is number 0-3"<<endl;
    cout<<"\t\t\t 0 for none [default]"<<endl;
    cout<<"\t\t\t 1 for nonlinear filter"<<endl;
    cout<<"\t\t\t 2 for nonlinear filter with median correction"<<endl;
    cout<<"\t\t\t 3 for median filter"<<endl;

    cout<<"\t--prefilter-mask=n"<<endl;
    cout<<"\t\tselect prefilter mask, where n is odd number [5 by default]"<<endl;
    cout<<"\t--postfilter-mask=n"<<endl;
    cout<<"\t\tselect postfilter mask, where n is odd number [7 by default]"<<endl;
    cout<<"\t--precision=n"<<endl;
    cout<<"\t\tselect output image precision, possible values 8 and 16 bit per channel"<<endl;

    cout<<"Misc options:"<<endl;
    cout<<"\t--verbose\t additional output"<<endl;
    cout<<"\t--help\t\t output this info"<<endl;
}
