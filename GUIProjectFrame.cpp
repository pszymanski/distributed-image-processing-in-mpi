//(*InternalHeaders(GUIProjectFrame)
#include <wx/intl.h>
#include <wx/string.h>
//*)

#include "GUIProjectFrame.h"
#include <vector>
#include <iostream>
#include <cstdlib>
#include "getopt.h"
#include "SerializationObjects.h"
#include <fstream>
#include "AsioCommunication.h"
#include "PngReaderWriter.h"
#include <wx/gdicmn.h>
#include <wx/mstream.h>



//(*IdInit(GUIProjectFrame)
const long GUIProjectFrame::ID_STATICTEXT1 = wxNewId();
const long GUIProjectFrame::ID_CHOICE1 = wxNewId();
const long GUIProjectFrame::ID_STATICTEXT2 = wxNewId();
const long GUIProjectFrame::ID_CHOICE2 = wxNewId();
const long GUIProjectFrame::ID_STATICTEXT3 = wxNewId();
const long GUIProjectFrame::ID_CHOICE3 = wxNewId();
const long GUIProjectFrame::ID_STATICTEXT4 = wxNewId();
const long GUIProjectFrame::ID_CHOICE4 = wxNewId();
const long GUIProjectFrame::ID_STATICTEXT5 = wxNewId();
const long GUIProjectFrame::ID_CHOICE5 = wxNewId();
const long GUIProjectFrame::ID_STATICTEXT6 = wxNewId();
const long GUIProjectFrame::ID_SPINCTRL1 = wxNewId();
const long GUIProjectFrame::ID_STATICTEXT7 = wxNewId();
const long GUIProjectFrame::ID_SPINCTRL2 = wxNewId();
const long GUIProjectFrame::ID_STATICTEXT8 = wxNewId();
const long GUIProjectFrame::ID_CHOICE6 = wxNewId();
const long GUIProjectFrame::ID_STATICTEXT9 = wxNewId();
const long GUIProjectFrame::ID_CHOICE7 = wxNewId();
const long GUIProjectFrame::ID_BUTTON1 = wxNewId();
const long GUIProjectFrame::ID_BUTTON2 = wxNewId();
const long GUIProjectFrame::ID_BUTTON3 = wxNewId();
const long GUIProjectFrame::ID_TEXTCTRL1 = wxNewId();
const long GUIProjectFrame::ID_STATICBITMAP1 = wxNewId();
const long GUIProjectFrame::ID_MESSAGEDIALOG1 = wxNewId();
//*)

BEGIN_EVENT_TABLE(GUIProjectFrame,wxFrame)
	//(*EventTable(GUIProjectFrame)
	//*)
END_EVENT_TABLE()


GUIProjectFrame::GUIProjectFrame(wxWindow* parent,wxWindowID id,const wxPoint& pos,const wxSize& size)
{
	//(*Initialize(GUIProjectFrame)
	wxFlexGridSizer* FlexGridSizer4;
	wxGridBagSizer* GridBagSizer1;
	wxFlexGridSizer* FlexGridSizer3;
	wxFlexGridSizer* FlexGridSizer5;
	wxFlexGridSizer* FlexGridSizer9;
	wxFlexGridSizer* FlexGridSizer2;
	wxFlexGridSizer* FlexGridSizer7;
	wxFlexGridSizer* FlexGridSizer8;
	wxFlexGridSizer* FlexGridSizer13;
	wxFlexGridSizer* FlexGridSizer12;
	wxFlexGridSizer* FlexGridSizer6;
	wxFlexGridSizer* FlexGridSizer1;
	wxFlexGridSizer* FlexGridSizer11;

	Create(parent, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxDEFAULT_FRAME_STYLE, _T("wxID_ANY"));
	SetClientSize(wxSize(853,754));
	FlexGridSizer1 = new wxFlexGridSizer(0, 3, 0, 0);
	GridBagSizer1 = new wxGridBagSizer(0, 0);
	FlexGridSizer2 = new wxFlexGridSizer(0, 3, 0, 0);
	StaticText1 = new wxStaticText(this, ID_STATICTEXT1, _("Demosaic method:"), wxDefaultPosition, wxSize(116,18), 0, _T("ID_STATICTEXT1"));
	FlexGridSizer2->Add(StaticText1, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	Choice1 = new wxChoice(this, ID_CHOICE1, wxDefaultPosition, wxDefaultSize, 0, 0, 0, wxDefaultValidator, _T("ID_CHOICE1"));
	Choice1->Append(_("Nearest Neighbor"));
	Choice1->Append(_("Bilinear"));
	Choice1->Append(_("Simple Gradients"));
	Choice1->SetSelection( Choice1->Append(_("VNG  Demosaic")) );
	FlexGridSizer2->Add(Choice1, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	GridBagSizer1->Add(FlexGridSizer2, wxGBPosition(0, 0), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	FlexGridSizer3 = new wxFlexGridSizer(0, 3, 0, 0);
	StaticText2 = new wxStaticText(this, ID_STATICTEXT2, _("Prefiltering method:"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT2"));
	FlexGridSizer3->Add(StaticText2, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	Choice2 = new wxChoice(this, ID_CHOICE2, wxDefaultPosition, wxDefaultSize, 0, 0, 0, wxDefaultValidator, _T("ID_CHOICE2"));
	Choice2->SetSelection( Choice2->Append(_("None")) );
	Choice2->Append(_("Statistic Filtering"));
	Choice2->Append(_("Median Filtering"));
	FlexGridSizer3->Add(Choice2, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	GridBagSizer1->Add(FlexGridSizer3, wxGBPosition(1, 0), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	FlexGridSizer4 = new wxFlexGridSizer(0, 3, 0, 0);
	StaticText3 = new wxStaticText(this, ID_STATICTEXT3, _("Postfilter method:"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT3"));
	FlexGridSizer4->Add(StaticText3, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	Choice3 = new wxChoice(this, ID_CHOICE3, wxDefaultPosition, wxSize(253,27), 0, 0, 0, wxDefaultValidator, _T("ID_CHOICE3"));
	Choice3->SetSelection( Choice3->Append(_("None")) );
	Choice3->Append(_("Nonlinear filter"));
	Choice3->Append(_("Nonlinear filter with median correction"));
	Choice3->Append(_("Nonlinear filter with bilinear interpolation"));
	Choice3->Append(_("Median filter"));
	FlexGridSizer4->Add(Choice3, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	GridBagSizer1->Add(FlexGridSizer4, wxGBPosition(2, 0), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	FlexGridSizer5 = new wxFlexGridSizer(0, 3, 0, 0);
	StaticText4 = new wxStaticText(this, ID_STATICTEXT4, _("Color matrix handling method:"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT4"));
	FlexGridSizer5->Add(StaticText4, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	Choice4 = new wxChoice(this, ID_CHOICE4, wxDefaultPosition, wxDefaultSize, 0, 0, 0, wxDefaultValidator, _T("ID_CHOICE4"));
	Choice4->Append(_("None"));
	Choice4->SetSelection( Choice4->Append(_("Dcraw default")) );
	FlexGridSizer5->Add(Choice4, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	GridBagSizer1->Add(FlexGridSizer5, wxGBPosition(3, 0), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	FlexGridSizer6 = new wxFlexGridSizer(0, 3, 0, 0);
	StaticText5 = new wxStaticText(this, ID_STATICTEXT5, _("Output image precision:"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT5"));
	FlexGridSizer6->Add(StaticText5, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	Precision = new wxChoice(this, ID_CHOICE5, wxDefaultPosition, wxDefaultSize, 0, 0, 0, wxDefaultValidator, _T("ID_CHOICE5"));
	Precision->SetSelection( Precision->Append(_("8 bit")) );
	Precision->Append(_("16 bit"));
	Precision->SetToolTip(_("select output image precision, possible values 8 and 16 bit per channel"));
	FlexGridSizer6->Add(Precision, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	GridBagSizer1->Add(FlexGridSizer6, wxGBPosition(4, 0), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	FlexGridSizer7 = new wxFlexGridSizer(0, 3, 0, 0);
	StaticText6 = new wxStaticText(this, ID_STATICTEXT6, _("Black level:"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT6"));
	FlexGridSizer7->Add(StaticText6, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	SpinCtrl1 = new wxSpinCtrl(this, ID_SPINCTRL1, _T("32"), wxDefaultPosition, wxDefaultSize, 0, 0, 500, 32, _T("ID_SPINCTRL1"));
	SpinCtrl1->SetValue(_T("32"));
	SpinCtrl1->SetToolTip(_("arbitary set black level value, where n is unsigned integer"));
	FlexGridSizer7->Add(SpinCtrl1, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	GridBagSizer1->Add(FlexGridSizer7, wxGBPosition(5, 0), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	FlexGridSizer8 = new wxFlexGridSizer(0, 3, 0, 0);
	StaticText7 = new wxStaticText(this, ID_STATICTEXT7, _("Saturation level:"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT7"));
	FlexGridSizer8->Add(StaticText7, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	SpinCtrl2 = new wxSpinCtrl(this, ID_SPINCTRL2, _T("1000"), wxDefaultPosition, wxDefaultSize, 0, 1000, 10000, 1000, _T("ID_SPINCTRL2"));
	SpinCtrl2->SetValue(_T("1000"));
	SpinCtrl2->SetToolTip(_("arbitary set saturation level value, where n is unsigned integer"));
	FlexGridSizer8->Add(SpinCtrl2, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	GridBagSizer1->Add(FlexGridSizer8, wxGBPosition(6, 0), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	FlexGridSizer12 = new wxFlexGridSizer(0, 3, 0, 0);
	StaticText8 = new wxStaticText(this, ID_STATICTEXT8, _("Prefilter mask (odd numbers):"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT8"));
	FlexGridSizer12->Add(StaticText8, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	PrefilterMaskChoice = new wxChoice(this, ID_CHOICE6, wxDefaultPosition, wxDefaultSize, 0, 0, 0, wxDefaultValidator, _T("ID_CHOICE6"));
	PrefilterMaskChoice->Append(_("3"));
	PrefilterMaskChoice->SetSelection( PrefilterMaskChoice->Append(_("5")) );
	PrefilterMaskChoice->Append(_("7"));
	PrefilterMaskChoice->Append(_("9"));
	FlexGridSizer12->Add(PrefilterMaskChoice, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	GridBagSizer1->Add(FlexGridSizer12, wxGBPosition(7, 0), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	FlexGridSizer9 = new wxFlexGridSizer(0, 3, 0, 0);
	StaticText9 = new wxStaticText(this, ID_STATICTEXT9, _("Postfilter mask (odd numbers):"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT9"));
	FlexGridSizer9->Add(StaticText9, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	PostfilterMaskChoice = new wxChoice(this, ID_CHOICE7, wxDefaultPosition, wxDefaultSize, 0, 0, 0, wxDefaultValidator, _T("ID_CHOICE7"));
	PostfilterMaskChoice->Append(_("3"));
	PostfilterMaskChoice->Append(_("5"));
	PostfilterMaskChoice->SetSelection( PostfilterMaskChoice->Append(_("7")) );
	PostfilterMaskChoice->Append(_("9"));
	FlexGridSizer9->Add(PostfilterMaskChoice, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	GridBagSizer1->Add(FlexGridSizer9, wxGBPosition(8, 0), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	FlexGridSizer11 = new wxFlexGridSizer(0, 3, 0, 0);
	LoadDngButton = new wxButton(this, ID_BUTTON1, _("Load DNG"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_BUTTON1"));
	FlexGridSizer11->Add(LoadDngButton, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	ProcessDngButton = new wxButton(this, ID_BUTTON2, _("Process DNG"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_BUTTON2"));
	FlexGridSizer11->Add(ProcessDngButton, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	GridBagSizer1->Add(FlexGridSizer11, wxGBPosition(9, 0), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	FlexGridSizer13 = new wxFlexGridSizer(0, 3, 0, 0);
	SaveProcessedFileButton = new wxButton(this, ID_BUTTON3, _("Save processed file"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_BUTTON3"));
	FlexGridSizer13->Add(SaveProcessedFileButton, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	GridBagSizer1->Add(FlexGridSizer13, wxGBPosition(11, 0), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	Address = new wxTextCtrl(this, ID_TEXTCTRL1, _("127.0.0.1"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_TEXTCTRL1"));
	GridBagSizer1->Add(Address, wxGBPosition(12, 0), wxDefaultSpan, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	FlexGridSizer1->Add(GridBagSizer1, 1, wxALL|wxEXPAND|wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL, 5);
	MemStaticBitmap = new wxStaticBitmap(this, ID_STATICBITMAP1, wxNullBitmap, wxDefaultPosition, wxSize(800,600), wxSIMPLE_BORDER|wxVSCROLL|wxHSCROLL, _T("ID_STATICBITMAP1"));
	FlexGridSizer1->Add(MemStaticBitmap, 1, wxALL|wxEXPAND|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	SetSizer(FlexGridSizer1);
	FileDialog1 = new wxFileDialog(this, _("Select file"), wxEmptyString, wxEmptyString, _("DNG, CR2 Files|*.dng;*.DNG;*.cr2;*.CR2"), wxFD_DEFAULT_STYLE, wxDefaultPosition, wxDefaultSize, _T("wxFileDialog"));
	MessageDialog1 = new wxMessageDialog(this, wxEmptyString, _("Message:"), wxOK, wxDefaultPosition);
	SetSizer(FlexGridSizer1);
	Layout();

	Connect(ID_BUTTON1,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&GUIProjectFrame::OnLoadDngButtonClick);
	Connect(ID_BUTTON2,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&GUIProjectFrame::OnProcessDngButtonClick);
	Connect(ID_BUTTON3,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&GUIProjectFrame::OnSaveProcessedFileButtonClick);
	//*)
}

GUIProjectFrame::~GUIProjectFrame()
{
	//(*Destroy(GUIProjectFrame)
	//*)
  if(imageChar!=nullptr)
    delete imageChar;
}


void GUIProjectFrame::OnLoadDngButtonClick(wxCommandEvent& event)
{
        int d = FileDialog1->ShowModal();
        if(d==wxID_OK) {
            filePath=FileDialog1->GetPath();
        }
        //else
        //    MessageDialog1->SetMessage("Fuck you! >:[");
       // MessageDialog1->ShowModal();
}

void GUIProjectFrame::OnProcessDngButtonClick(wxCommandEvent& event)
{
    InputData inputSettings=createInputData();
    communication(inputSettings);
}

void GUIProjectFrame::communication(InputData inputSettings) {
            AsioCommunication comm;
            try
              {
                comm.connectAsClient(Address->GetLineText(0).ToStdString());
                comm.sendObject(inputSettings, messageType::inputData);
                cout<<"send"<<endl;

                Header m=comm.readHeader();
                uint32_t id=m.value;
                cout<<id<<endl;
                m=comm.readHeader();
                comm.finishedId(id);
                cout<<"query"<<endl;

                m=comm.readHeader();
                OutputData out = comm.readObject<OutputData>(m.value);
                cout<<"read"<<endl;

                PngReaderWriter writer;
                if(imageChar!=nullptr)
                  delete imageChar;
                imageChar= new WorkingImage<unsigned char, 3>(out.realWidth, out.realHeight);
                imageChar->fill(0,0,0);
                unsigned int cumulativeSize=0;
                while(true)
                  {
                    pair<int, int> WH = writer.readPngFromMemory(*imageChar, out);
                    cumulativeSize+=WH.first*WH.second;
                    if(cumulativeSize==out.realHeight*out.realWidth)
                      break;
                    m=comm.readHeader();
                    out = comm.readObject<OutputData>(m.value);
                  }

                wxImage imgmem(out.realWidth,out.realHeight);
                imgmem.SetData(imageChar->singleMemoryBlock, true); //nie zwalniaj tego bufora w destruktorze
                wxBitmap bmpmem(imgmem.Scale(MemStaticBitmap->GetSize().GetWidth(), MemStaticBitmap->GetSize().GetHeight()), -1);
                MemStaticBitmap->SetBitmap(bmpmem);
              }
            catch (const boost::system::system_error& ex)
              {
                MessageDialog1->SetMessage(ex.what());
                MessageDialog1->ShowModal();
              }
            catch (const std::exception& e)
              {
                MessageDialog1->SetMessage(e.what());
                MessageDialog1->ShowModal();
              }
}

InputData GUIProjectFrame::createInputData() {
        std::ifstream file(filePath.char_str(), std::ios::binary);
        // read the data:
        std::vector<char> dngBuffer= std::vector<char>((std::istreambuf_iterator<char>(file)),
                              std::istreambuf_iterator<char>());
        int demosaic,colorMatrix, prefilter, postfilter;
        int black,saturation,prefilterMask,postfilterMask;
        demosaic=Choice1->GetSelection();
        prefilter=Choice2->GetSelection();
        postfilter=Choice3->GetSelection();
        colorMatrix=Choice4->GetSelection();
        black=SpinCtrl1->GetValue();
        saturation = SpinCtrl2->GetValue();
        prefilterMask=atoi(PrefilterMaskChoice->GetString(PrefilterMaskChoice->GetSelection()).fn_str());
        postfilterMask=atoi(PostfilterMaskChoice->GetString(PostfilterMaskChoice->GetSelection()).fn_str());
        InputData inputSettings;
        inputSettings.buffer=dngBuffer;
        inputSettings.postfilterMask=postfilterMask;
        inputSettings.postfilter=static_cast<postfilterType>(postfilter);
        inputSettings.prefilterMask=prefilterMask;
        inputSettings.prefilter=static_cast<prefilterType>(prefilter);
        inputSettings.blackLevel=black;
        inputSettings.customBlackLevel=true;
        inputSettings.colorMatrix=static_cast<colorMatrixType>(colorMatrix);
        inputSettings.demosaic=static_cast<demosaicMethod>(demosaic);
        inputSettings.saturationLevel=saturation;
        inputSettings.customSaturationLevel=true;

        switch(Precision->GetSelection())
          {
          case 0:
            inputSettings.precision=8;
            break;

          case 1:
            inputSettings.precision=16;
            break;
          }

        return inputSettings;
}



void GUIProjectFrame::OnSaveProcessedFileButtonClick(wxCommandEvent& event)
{
  if(imageChar==nullptr)
    return;
  
  PngReaderWriter writer;
  std::vector<char>  buffer = writer.writePngToMemory(*imageChar, imageChar->getPrecision());
  ofstream os2 (filePath.BeforeLast('.')+".png", ofstream::binary);
  if (os2)
    {
      os2.write(buffer.data(), buffer.size());
    }
}
