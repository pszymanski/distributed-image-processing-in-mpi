#include "PngReaderWriter.h"
#include <png.h>
#include <iostream>
#include <cstring>

using namespace std;
PngReaderWriter::PngReaderWriter()
{
}
PngReaderWriter::~PngReaderWriter()
{
}
static void PngWriteCallback(png_structp png_ptr, png_bytep data,png_size_t length)
{
    vector<char> *p = (vector<char>*) png_get_io_ptr(png_ptr);
    p->insert(p->end(), data, data + length);
}
static void PngReadCallback(png_structp png_ptr, png_bytep data, png_size_t length)
{
    PngReaderWrapper *p = (PngReaderWrapper*) png_get_io_ptr(png_ptr);
    std::copy(p->buffer.data()+p->offset, p->buffer.data()+p->offset+length,data);
    p-> offset+=length;

}
vector<char> PngReaderWriter::writePngToMemory(WorkingImage<arrayType, 3> & image,int precision)
{
    int height = image.height;
    int width = image.width;
    int imgPrecision = image.getPrecision();
    if(imgPrecision<precision)
        precision=imgPrecision;//we cannot convert 8 bit image to 16 bit.
    unsigned short * inputBuffer = image.singleMemoryBlock;
    uint8_t ** outputRows = new uint8_t*[height];
    int outputBufferSize=width * height * 3;
    if(precision==16)
        outputBufferSize*=2;
    uint8_t * outputBuffer = new uint8_t[outputBufferSize];
    int l = 0, m=0;

    //RGBG - Copy to Image
    for (uint j = 0; j < height; j++)
    {
        outputRows[j] = &outputBuffer[m];
        for (uint i = 0; i < width; i++)
        {
            for (uint k = 0; k < 3; k++)
            {
                int value;
                if(image.isLinearRGB())
                    value = LUT2[inputBuffer[l]];
                else
                    value= inputBuffer[l];
                if(imgPrecision==8)
                {
                    outputBuffer[m] = value;
                    m++;
                }
                else
                {
                    outputBuffer[m] = value>>8;
                    m++;
                    if(precision==16)
                    {
                        outputBuffer[m] = value;
                        m++;
                    }
                }
                l++;

            }
        }
    }
    return writePngToMemory(outputRows, width, height, precision);
}
vector<char> PngReaderWriter::writePngToMemory(WorkingImage<unsigned char, 3> & image,int precision)
{
    int height = image.height;
    int width = image.width;
    int imgPrecision = image.getPrecision();
    if(imgPrecision<precision)
        precision=imgPrecision;//we cannot convert 8 bit image to 16 bit.
    unsigned char * inputBuffer = image.singleMemoryBlock;
    uint8_t ** outputRows = new uint8_t*[height];
    int outputBufferSize=width * height * 3;
    if(precision==16)
        outputBufferSize*=2;
    uint8_t * outputBuffer = new uint8_t[outputBufferSize];
    int l = 0, m=0;

    //RGBG - Copy to Image
    for (uint j = 0; j < height; j++)
    {
        outputRows[j] = &outputBuffer[m];
        for (uint i = 0; i < width; i++)
        {
            for (uint k = 0; k < 3; k++)
            {
                outputBuffer[m] = inputBuffer[l];
                m++;
                l++;

            }
        }
    }
    return writePngToMemory(outputRows, width, height, precision);
}
vector<char> PngReaderWriter::writePngToMemory(unsigned char** outputBuffer, int width,
        int height, int precision)
{
    png_structp png_ptr; //data structure
    png_infop info_ptr; //info structure
    vector<char> out;
    // Initialize write structure
    if ((png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, nullptr,
                                           nullptr, nullptr)) == nullptr)
    {
        logger.log("Could not allocate write struct");
        throw exception(); ///TODO Better exception handling
    }

    // Initialize info structure
    if ((info_ptr = png_create_info_struct(png_ptr)) == nullptr)
    {
        logger.log("Could not allocate info struct");
        throw exception();
    }

    if (setjmp(png_jmpbuf(png_ptr)))
    {
        logger.log("Error during png creation");
        throw exception();
    }

    png_set_IHDR(png_ptr, info_ptr, width, height, precision, PNG_COLOR_TYPE_RGB,
                 PNG_INTERLACE_NONE,
                 PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);
    png_set_rows(png_ptr, info_ptr, outputBuffer);
    png_set_write_fn(png_ptr, &out, PngWriteCallback, NULL);
    png_write_png(png_ptr, info_ptr, PNG_TRANSFORM_IDENTITY, nullptr);
    //save changes
    if (setjmp(png_jmpbuf(png_ptr)))
    {
        logger.log("Error during png creation");
        throw exception();
    }
    png_destroy_write_struct(&png_ptr, &info_ptr);
    return out;
}
pair<int, int> PngReaderWriter::readPngFromMemory(WorkingImage<arrayType, 3> &image, OutputData data)
{
    png_structp png_ptr; //data structure
    png_infop info_ptr; //info structure
    // Initialize write structure
    if ((png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, nullptr,
                                          nullptr, nullptr)) == nullptr)
    {
        logger.log("Could not allocate read struct");
        throw exception(); ///TODO Better exception handling
    }

    // Initialize info structure
    if ((info_ptr = png_create_info_struct(png_ptr)) == nullptr)
    {
        logger.log("Could not allocate info struct");
        throw exception();
    }

    if (setjmp(png_jmpbuf(png_ptr)))
    {
        logger.log("Error during png read");
        throw exception();
    }
    PngReaderWrapper wrapper;
    wrapper.buffer=data.buffer;
    png_set_read_fn(png_ptr, &wrapper, PngReadCallback);
    png_read_info(png_ptr, info_ptr);

    png_uint_32 width = 0;
    png_uint_32 height = 0;
    int bitDepth = 0;
    int colorType = -1;
    png_uint_32 retval = png_get_IHDR(png_ptr, info_ptr,
                                      &width,
                                      &height,
                                      &bitDepth,
                                      &colorType,
                                      NULL, NULL, NULL);
    if(retval != 1)
    {
        logger.log("Error during png header read");
        throw exception();
    }

    if(colorType!=PNG_COLOR_TYPE_RGB)
    {
        logger.log("Other modes than RGB is not supported!");
        throw exception();
    }

    image.setPrecision(bitDepth);
    image.setLinearRGB(false);
    //cout<<bitDepth;
    int rowSize=png_get_rowbytes(png_ptr, info_ptr);
    uint8_t * rowData = new uint8_t[rowSize];
    // read single row at a time
    for(int i = 0; i < height; i++)
    {
        png_read_row(png_ptr, rowData, NULL);
        int rowindex=0;
        for(int j= 0; j<width; j++)
            for (uint k = 0; k < 3; k++)
            {
                arrayType value =rowData[rowindex++];
                if(bitDepth==16)
                {
                    value=(value<<8) +rowData[rowindex++];
                }
                image.set(i+data.startingHeight, j+data.startingWidth, k, value);
            }
    }
    //save changes
    if (setjmp(png_jmpbuf(png_ptr)))
    {
        logger.log("Error during png read finalize");
        throw exception();
    }
    png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
    pair<int, int> WH(width, height);
    return WH;
}
pair<int, int> PngReaderWriter::readPngFromMemory(WorkingImage<unsigned char, 3> &image, OutputData data)
{
    png_structp png_ptr; //data structure
    png_infop info_ptr; //info structure
    // Initialize write structure
    if ((png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, nullptr,
                                          nullptr, nullptr)) == nullptr)
    {
        logger.log("Could not allocate read struct");
        throw exception(); ///TODO Better exception handling
    }

    // Initialize info structure
    if ((info_ptr = png_create_info_struct(png_ptr)) == nullptr)
    {
        logger.log("Could not allocate info struct");
        throw exception();
    }

    if (setjmp(png_jmpbuf(png_ptr)))
    {
        logger.log("Error during png read");
        throw exception();
    }
    PngReaderWrapper wrapper;
    wrapper.buffer=data.buffer;
    png_set_read_fn(png_ptr, &wrapper, PngReadCallback);
    png_read_info(png_ptr, info_ptr);

    png_uint_32 width = 0;
    png_uint_32 height = 0;
    int bitDepth = 0;
    int colorType = -1;
    png_uint_32 retval = png_get_IHDR(png_ptr, info_ptr,
                                      &width,
                                      &height,
                                      &bitDepth,
                                      &colorType,
                                      NULL, NULL, NULL);
    if(retval != 1)
    {
        logger.log("Error during png header read");
        throw exception();
    }

    if(colorType!=PNG_COLOR_TYPE_RGB)
    {
        logger.log("Other modes than RGB is not supported!");
        throw exception();
    }

    image.setPrecision(8);
    image.setLinearRGB(false);
    //cout<<bitDepth;
    int rowSize=png_get_rowbytes(png_ptr, info_ptr);
    uint8_t * rowData = new uint8_t[rowSize];
    // read single row at a time
    for(int i = 0; i < height; i++)
    {
        png_read_row(png_ptr, rowData, NULL);
        int rowindex=0;
        for(int j= 0; j<width; j++)
            for (uint k = 0; k < 3; k++)
            {
                arrayType value =rowData[rowindex++];
                if(bitDepth==16)
                {
                    rowindex++;
                }
                image.set(i+data.startingHeight, j+data.startingWidth, k, value);
            }
    }
    //save changes
    if (setjmp(png_jmpbuf(png_ptr)))
    {
        logger.log("Error during png read finalize");
        throw exception();
    }
    png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
    pair<int, int> WH(width, height);
    return WH;
}
//Save using old API
void PngReaderWriter::save(unsigned char** outputBuffer, int width, int height, int precision,
                           string filename)
{
    FILE *fp; //pointer to file
    png_structp png_ptr; //data structure
    png_infop info_ptr; //info structure

    if ((fp = fopen(filename.c_str(), "wb")) == nullptr) //open file
    {
        logger.log((boost::format("Could not open file %s for writing") %filename.c_str()).str());
        return;
    }

    // Initialize write structure
    if ((png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, nullptr,
                                           nullptr, nullptr)) == nullptr)
    {
        logger.log("Could not allocate write struct");
        return;
    }

    // Initialize info structure
    if ((info_ptr = png_create_info_struct(png_ptr)) == nullptr)
    {
        logger.log("Could not allocate info struct");
        return;
    }

    if (setjmp(png_jmpbuf(png_ptr)))
    {
        logger.log("Error during png creation");
        return;
    }
    png_init_io(png_ptr, fp);
    //disable compression
    //png_set_compression_level(png_ptr,
    //                         Z_NO_COMPRESSION);
    // Write header (8 bit color depth)
    png_set_IHDR(png_ptr, info_ptr, width, height, precision, PNG_COLOR_TYPE_RGB,
                 PNG_INTERLACE_NONE,
                 PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);
    //set data
    png_set_rows(png_ptr, info_ptr, outputBuffer);
    //write changes
    png_write_png(png_ptr, info_ptr, PNG_TRANSFORM_IDENTITY, nullptr);
    //save changes
    if (setjmp(png_jmpbuf(png_ptr)))
    {
        logger.log("Error during png creation");
    }
    //do cleanup
    png_destroy_write_struct(&png_ptr, &info_ptr);
    fclose(fp);
    return;
}
WorkingImage<arrayType, 3> * PngReaderWriter::read(string file)
{
    png_image image;
    memset(&image, 0, sizeof image);
    image.version = PNG_IMAGE_VERSION;
    if (!png_image_begin_read_from_file(&image, file.c_str()))
        return nullptr;
    image.format = PNG_FORMAT_LINEAR_RGB; //libpng do conversion to sRGB by itself
    int image_size = PNG_IMAGE_SIZE(image);
    logger.log((boost::format("size is %d %s") %image_size%image.message).str());
    logger.log((boost::format("%d %d") %image.height%image.width).str());
    png_bytep * buffer = new png_bytep[image_size];
    png_image_finish_read(&image, NULL/*background*/, buffer, 0/*row_stride*/,
                          NULL/*colormap for PNG_FORMAT_FLAG_COLORMAP */);
    WorkingImage<ushort, 3> * temp = new WorkingImage<ushort, 3>(
        (ushort*) buffer, image.width, image.height);
    WorkingImage<arrayType, 3> * ret = new WorkingImage<arrayType, 3>(temp);
    delete temp;
    return ret;
}
//Save using new API
void PngReaderWriter::saveNew(unsigned short* outputBuffer, int width,
                              int height, int precision, string filename)
{
    png_image image;
    memset(&image, 0, sizeof image);
    image.format = PNG_FORMAT_LINEAR_RGB; //libpng do conversion to sRGB by itself
    image.height = height;
    image.width = width;
    image.version = PNG_IMAGE_VERSION;
    if (png_image_write_to_file(&image, filename.c_str(),
                                precision==16/*convert_to_8bit*/, outputBuffer, 0/*row_stride - byte order*/,
                                nullptr) == 0)
        logger.log((boost::format("pngtopng: write %s: %s") %filename.c_str()%image.message).str());
    return;
}
////getters/setters
//int PngReaderWriter::getPrecision()
//{
//    return precision;
//}
//void PngReaderWriter::setPrecision(int precision)
//{
//    if (precision == 8 || precision == 16)
//        this->precision = precision;
//}
