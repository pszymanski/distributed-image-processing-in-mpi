#ifndef ISO_HPP
#define ISO_HPP

#include "SerializationObjects.h"

template<typename T>
class IdentifiedData
{
 public:
  uint32_t Id;
  T Data;

  IdentifiedData(){};
  IdentifiedData(uint32_t id, const T& data):Id(id),Data(data){};

  template <typename Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
      ar & Id;
      ar & Data;
    }

  T* operator->()
  {
    return &Data;
  }

};

#endif
