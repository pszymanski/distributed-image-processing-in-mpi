#ifndef GUIPROJECTFRAME_H
#define GUIPROJECTFRAME_H

#include <SerializationObjects.h>
#include "WorkingImage.h"

//(*Headers(GUIProjectFrame)
#include <wx/sizer.h>
#include <wx/stattext.h>
#include <wx/textctrl.h>
#include <wx/msgdlg.h>
#include <wx/spinctrl.h>
#include <wx/filedlg.h>
#include <wx/choice.h>
#include <wx/statbmp.h>
#include <wx/gbsizer.h>
#include <wx/button.h>
#include <wx/frame.h>
//*)

class GUIProjectFrame: public wxFrame
{
	public:

		GUIProjectFrame(wxWindow* parent,wxWindowID id=wxID_ANY,const wxPoint& pos=wxDefaultPosition,const wxSize& size=wxDefaultSize);
		virtual ~GUIProjectFrame();

		//(*Declarations(GUIProjectFrame)
		wxStaticText* StaticText9;
		wxTextCtrl* Address;
		wxChoice* PostfilterMaskChoice;
		wxStaticBitmap* MemStaticBitmap;
		wxSpinCtrl* SpinCtrl1;
		wxStaticText* StaticText2;
		wxChoice* Choice3;
		wxStaticText* StaticText6;
		wxChoice* Precision;
		wxStaticText* StaticText8;
		wxButton* ProcessDngButton;
		wxStaticText* StaticText1;
		wxFileDialog* FileDialog1;
		wxStaticText* StaticText3;
		wxChoice* PrefilterMaskChoice;
		wxStaticText* StaticText5;
		wxStaticText* StaticText7;
		wxChoice* Choice4;
		wxSpinCtrl* SpinCtrl2;
		wxMessageDialog* MessageDialog1;
		wxStaticText* StaticText4;
		wxButton* LoadDngButton;
		wxChoice* Choice1;
		wxButton* SaveProcessedFileButton;
		wxChoice* Choice2;
		//*)

	protected:

		//(*Identifiers(GUIProjectFrame)
		static const long ID_STATICTEXT1;
		static const long ID_CHOICE1;
		static const long ID_STATICTEXT2;
		static const long ID_CHOICE2;
		static const long ID_STATICTEXT3;
		static const long ID_CHOICE3;
		static const long ID_STATICTEXT4;
		static const long ID_CHOICE4;
		static const long ID_STATICTEXT5;
		static const long ID_CHOICE5;
		static const long ID_STATICTEXT6;
		static const long ID_SPINCTRL1;
		static const long ID_STATICTEXT7;
		static const long ID_SPINCTRL2;
		static const long ID_STATICTEXT8;
		static const long ID_CHOICE6;
		static const long ID_STATICTEXT9;
		static const long ID_CHOICE7;
		static const long ID_BUTTON1;
		static const long ID_BUTTON2;
		static const long ID_BUTTON3;
		static const long ID_TEXTCTRL1;
		static const long ID_STATICBITMAP1;
		static const long ID_MESSAGEDIALOG1;
		//*)

	private:
        void communication(InputData inputData);
		InputData createInputData();
                WorkingImage<unsigned char, 3> * imageChar=nullptr;
                wxString filePath;

		//(*Handlers(GUIProjectFrame)
		void OnLoadDngButtonClick(wxCommandEvent& event);
		void OnProcessDngButtonClick(wxCommandEvent& event);
		void OnSaveSelectedButtonClick(wxCommandEvent& event);
		void OnDeleteSelectedButtonClick(wxCommandEvent& event);
		void OnSaveProcessedFileButtonClick(wxCommandEvent& event);
		//*)

		DECLARE_EVENT_TABLE()
};

#endif
