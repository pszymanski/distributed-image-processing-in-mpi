#ifndef WRITEIMAGE_H
#define WRITEIMAGE_H
#include <iostream>
#include "WorkingImage.h"
#include <vector>
#include <cmath>
#include "GammaLUT.h"
#include "Logger.h"
#include "SerializationObjects.h"
struct PngReaderWrapper{
public:
    std::vector<char> buffer;
    int offset=0;
};
class PngReaderWriter
{
public:
    PngReaderWriter();
    ~PngReaderWriter();
    WorkingImage<arrayType,3> * read(std::string file);
    void save(unsigned char **outputBuffer, int width, int height, int precision, std::string filename);
    void saveNew(unsigned short* outputBuffer, int width, int height, int precision, std::string filename);
    ///getters/setters
    int getPrecision();
    void setPrecision(int precision);
    bool isLinearRGB();
    void setLinearRGB(bool linearRGB);
    ///LUT generation
    //static const int index2=1<<16;
//    unsigned short * createLUT(int GAMMA)
//    {
//        double gamma=GAMMA/10.0;
//        unsigned short * linearToGamma= new unsigned short[index2];
//        for(int i=0; i<index2; i++){
//            double value=pow(double(i)/index2, 1/gamma)*index2;
//            if(value>UINT16_MAX)
//                linearToGamma[i]=UINT16_MAX;
//            else
//                linearToGamma[i]=value;
//            }
//
//        return linearToGamma;
//
//    }
    //const unsigned short * LUT =createLUT(22);///that thing could be (also should be) generated at compile time. Can you do that?
    std::vector<char> writePngToMemory(unsigned char** outputBuffer, int width, int height, int precision);
    std::vector<char> writePngToMemory(WorkingImage<arrayType,3> & object, int precision);
    pair<int, int> readPngFromMemory(WorkingImage<arrayType, 3> &image, OutputData data);
    pair<int, int> readPngFromMemory(WorkingImage<unsigned char, 3> &image, OutputData data);
    vector<char> writePngToMemory(WorkingImage<unsigned char, 3> & image,int precision);
private:
    Logger<PngReaderWriter> logger = Logger<PngReaderWriter>::getInstance(this);
    //int precision=16;
};

#endif // WRITEIMAGE_H
