#include <iostream>
#include <cstdlib>
#include <iomanip>
#include "IdentifiedSerializationObjects.hpp"
#include "AsioCommunication.h"
#include "RawImage.h"
#include "LibRawWrapper.h"
#include "MPIWrapper.h"
#include "PartialRawWrapper.h"
#include <queue>
#include <utility> //std::pair
#include <list>
#include <thread>
#include <mutex>
#include <boost/system/error_code.hpp>
#include <chrono>
#include <thread>

using namespace boost;
using namespace std;

class Worker
{
public:

    Worker()
    {

    }
    ~Worker()
    {
        if(comm!=nullptr)
            delete comm;
        if(connectionThread!=nullptr)
            delete connectionThread;
        if(processThread!=nullptr)
            delete processThread;
    }

    void DoIt()
    {
        connectionThread=new thread(&Worker::Connection, ref(*this));
        connectionThread->detach();
        processThread=new thread(&Worker::Process, ref(*this));
        processThread->detach();
    }

    void Process()
    {
        for (;;)
        {
            try
            {
                int rank =0;
                PartialRawWrapper myPart;
                IdentifiedData<OutputData> output;
                ///if we have processes
                if(mpiW.getSize()!=1)
                {
                    ///coordinator should select process with largest queue and send it's id to other processes
                    ///they should wait for broadcast object from that id.
                    if(mpiW.getRank()==0)
                    {
                        vector<int> whoisReady;
                        int siz = inputQueue.size();
                        mpiW.gather(siz, whoisReady, 0); ///fill vector with queue sizes of processes in world
                        ///search for largest element
                        vector<int>::iterator iter = max_element(whoisReady.begin(), whoisReady.end());//std::find(whoisReady.begin(), whoisReady.end(), 1);
                        ///largest element will start processing
                        if (iter!=std::end(whoisReady)&& *iter > 0)
                            rank = iter-whoisReady.begin(); ///this should be smaller or equal to world size
                        else
                            rank = -1; ///if all processes have empty queue
                        //logger.log((format("root - %d")%rank).str());
                        mpiW.broadcast(rank,0);
                    }
                    else
                    {
                        int siz = inputQueue.size();
                        mpiW.gather(siz, 0);
                        //logger.log((format("slave - %d")%rank).str());
                        mpiW.broadcast(rank,0);
                        //logger.log((format("slave - %d")%rank).str());
                    }
                }
                if(rank==-1)
                    continue;
                if(rank==mpiW.getRank())
                {
                    if(inputQueue.size()==0)
                        continue;
                    vector<PartialRawWrapper> parts;
                    IdentifiedData<InputData> input;
                    {
                        unique_lock<mutex> lock(inputMutex);
                        input=inputQueue.front();
                        inputQueue.pop();
                    }
                    LibRawWrapper rawWrapper(input.Data);
                    int isOk=rawWrapper.open();
                    if(mpiW.getSize()!=1)
                    {
                        mpiW.broadcast(isOk, rank);
                    }
                    if(isOk!=0)
                    {
                        delete connectionThread;
                        resetConnection();
                        connectionThread=new thread(&Worker::Connection, ref(*this));
                        connectionThread->detach();
                        continue;
                    }
                    rawWrapper.doWhiteBalance();
                    rawWrapper.cropToActiveArea();
                    if(mpiW.getSize()!=1)
                    {
                        parts=PartialRawWrapper::split(rawWrapper, mpiW.getSize());
                        mpiW.scatter(myPart, parts, rank);
                    }
                    else
                    {
                        myPart.getCopy(rawWrapper);
                    }
                    output->realHeight = rawWrapper.height;
                    output->realWidth = rawWrapper.width;
                    output.Id=input.Id;

                }
                else
                {
                    int isOk=0;
                    mpiW.broadcast(isOk, rank);
                    cout<<isOk<<endl;
                    if(isOk!=0)
                        continue;
                    mpiW.scatter(myPart, rank);
                }
                myPart.cropToActiveArea();
                RAWImage RAWEngine(&myPart);
                RAWEngine.copyForProcess();
                RAWEngine.applyPrefilter();
                RAWEngine.demosaic();
                RAWEngine.applyPostfilter();
                int maxs = RAWEngine.findMax();
                //logger.log((format("%d")%maxs).str());
                if(mpiW.getSize()!=1)
                {
                    mpiW.globalMaximum(maxs);
                    //logger.log((format("%d")%maxs).str());
                    RAWEngine.insertMax(maxs);
                }
                RAWEngine.colorCorrection();

                RAWEngine.savePNG(output.Data);
                output->startingHeight = myPart.startingx;
                output->startingWidth = myPart.startingy;
                if(mpiW.getSize()!=1)
                {
                    if(rank==mpiW.getRank())
                    {
                        vector<OutputData> results;
                        mpiW.gather(output.Data, results, rank);
                        unique_lock<mutex> lock(outputMutex);
                        for(uint i=0; i<results.size(); i++)
                        {
                            OutputData obj= results[i];
                            obj.realHeight=output->realHeight;
                            obj.realWidth=output->realWidth;
                            outputQueue.push_back(IdentifiedData<OutputData>(output.Id, obj));
                        }

                        if(results.size()>0)
                            comm->finishedId(output.Id);

                    }
                    else
                    {
                        mpiW.gather(output.Data, rank);
                    }
                }
                else
                {
                    //                        ofstream os ("test.png", ofstream::binary);
                    //                        if (os)
                    //                        {
                    //                            os.write(output.buffer.data(), output.buffer.size());
                    //                        }
                    unique_lock<mutex> lock(outputMutex);
                    outputQueue.push_back(output);
                    comm->finishedId(output.Id);
                }
                cout<<"end!"<<endl;

            }
            catch (std::exception& e)
            {
                std::cerr <<"exception "<< e.what() << std::endl;
            }
        }
    }

    void Connection()
    {
        for (;;)
        {
            try
            {
                if(comm==nullptr)
                    resetConnection();

                Header m=comm->readHeader();

                if(m.type==messageType::inputData)
                {
                    InputData inputs = comm->readObject<InputData>(m.value);
                    unique_lock<mutex> lock(inputMutex);
                    inputQueue.push(IdentifiedData<InputData>(uniqueIdHack,inputs));
                    comm->broadcastId(uniqueIdHack);
                    uniqueIdHack++;
                }
                else if(m.type==messageType::queryId)
                {
                    bool resetConnectionW=false;
                    unique_lock<mutex> lock(outputMutex);
                    for(auto it=outputQueue.begin(); it!=outputQueue.end(); ++it)
                    {
                        if (it->Id!=m.value)
                            continue;
                        resetConnectionW=true; //skoro przynajmniej jedną rzecz wysłano, to trzeba będzie restartować połączenie
                        comm->sendObject(it->Data,messageType::outputData);
                        auto deleteIterator=it--; //chyba może być kilka wyników z takim samym id
                        outputQueue.erase(deleteIterator);
                    }

                    if(resetConnectionW)
                    {
                        resetConnection();
                    }
                }

            }

            catch (const boost::system::system_error& ex)
            {
                std::cerr <<"exception "<< ex.what() << std::endl;
                if(ex.code()==asio::error::address_in_use)
                    return;//To tylko MPI.
                if(connectionThread->get_id()==this_thread::get_id())
                    resetConnection();
                else
                    return;

            }
            catch (std::exception& e)
            {
                std::cerr <<"exception "<< e.what() << std::endl;
            }
        }
    }
private:
    AsioCommunication* comm=nullptr;
    Logger<Worker> logger = Logger<Worker>::getInstance(this);
    MPIWrapper mpiW;
    queue<IdentifiedData<InputData>> inputQueue;
    list<IdentifiedData<OutputData>> outputQueue;
    uint32_t uniqueIdHack=0;
    void resetConnection()
    {
        if(comm!=nullptr)
            delete comm;
        comm=new AsioCommunication();
        comm->connectAsServer();
    }
    mutex inputMutex;
    mutex outputMutex;
    thread* processThread=nullptr;
    thread* connectionThread=nullptr;

};
int main()
{
    Worker worker;
    worker.DoIt();
    for(;;);
}

