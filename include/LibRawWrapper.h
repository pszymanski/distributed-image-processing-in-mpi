#ifndef LIBRAWWRAPPER_H
#define LIBRAWWRAPPER_H
#include <libraw/libraw.h>
#include <iostream>
#include "RawWrapper.h"

class LibRawWrapper : public RawWrapper
{
public:
    LibRawWrapper();
    LibRawWrapper(InputData& input);
    //LibRawWrapper(LibRawWrapper& source);
    RawWrapper * copy();
    int maxValue();
    virtual ~LibRawWrapper();
    int open();
    void cropToActiveArea();
    void doWhiteBalance();
    boost::array<boost::array<double,3>,3> getColorArray();
protected:
    void decodeOffsets(int i);
private:
    Logger<LibRawWrapper> logger = Logger<LibRawWrapper>::getInstance(this);
    LibRaw iProcessor;
    void free();
};

#endif // LIBRAWWRAPPER_H
