#ifndef PSEUDOUI_H
#define PSEUDOUI_H

#include "getopt.h"
#include "SerializationObjects.h"
#include <fstream>
#include "PngReaderWriter.h"
#include <thread>
#include <condition_variable>
#include <mutex>
using namespace std;
class PseudoUI
{
public:
    void parseInput(int argc, char* argv[]);
    condition_variable imageReady;
    mutex mut;
    WorkingImage<arrayType, 3> * image=nullptr;
private:
    InputData inputSettings;
    string address="127.0.0.1";
    void printHelp();
};
#endif // PSEUDOUI_H
