/***************************************************************
 * Name:      GUIProjectApp.cpp
 * Purpose:   Code for Application Class
 * Author:     ()
 * Created:   2014-06-30
 * Copyright:  ()
 * License:
 **************************************************************/

#include "GUIProjectApp.h"
#include "GUIProjectFrame.h"

//(*AppHeaders
#include <wx/image.h>
//*)

IMPLEMENT_APP(GUIProjectApp);

bool GUIProjectApp::OnInit()
{
    //(*AppInitialize
    bool wxsOK = true;
    wxInitAllImageHandlers();
    if ( wxsOK )
    {
    	GUIProjectFrame* Frame = new GUIProjectFrame(0);
    	Frame->Show();
    	SetTopWindow(Frame);
    	wxImage::AddHandler(new wxPNGHandler);
    }
    //*)
    return wxsOK;

}
