#include "RawWrapper.h"
#include <cstring>
using namespace std;
RawWrapper::RawWrapper()
{
    //ctor
}

RawWrapper::~RawWrapper()
{
    free();

}
void RawWrapper::setExtendValue(){
    extend =2;
    if(internalSettings.prefilter!=prefilterType::none)
        extend = max(extend, (internalSettings.prefilterMask+1)/2);
    if(internalSettings.postfilter==postfilterType::impulseFilterWithMedian
            ||internalSettings.postfilter==postfilterType::impulseFilter
            ||internalSettings.postfilter==postfilterType::impulseFilterBilinear)
        extend = max(extend, (internalSettings.postfilterMask+1)/2);
    if(extend&1)
        extend++;
    cout<<"extend "<<extend<<endl;
}
void RawWrapper::free(){
    if(extendRawArray!=nullptr)
    {
        delete[] (extendRawArray-extend);
        extendRawArray=nullptr;
    }
}
void RawWrapper::buildArray(){
    //if(extendRawArray!=nullptr)
    //    delete[] (extendRawArray-extend);
    extendRawArray= new ushort*[height+(extend<<1)];
    ushort * src = extendedRawImage.get();
    for(int j=0, l=extend; j<height+(extend<<1); j++)
    {
        extendRawArray[j]=&src[l];
        l+=width+(extend<<1);
    }
    extendRawArray=extendRawArray+extend;
}


ushort * RawWrapper::extendBayerArea(ushort* data, int extendBlocks)
{
    ushort * newArray = new ushort[(height+(extendBlocks<<2))*(width+(extendBlocks<<2))];
    int blockWidth=width;
    //copy first rows
    ushort * src= data+(blockWidth*(extendBlocks-1)<<1);//data
    ushort * dest=newArray;
    for(int j=0; j<extendBlocks; j++)
    {
        src+=(extendBlocks-1)<<1;//0
        for(int i=0; i<extendBlocks; i++)
        {
            std::copy(src, src+2, dest);
            dest+=2;
            src-=2;//-2
        }
        src+=2;//0
        std::copy(src, src+blockWidth, dest);
        dest+=blockWidth;
        src+=blockWidth;//width
        src-=2;//width-2
        for(int i=0; i<extendBlocks; i++)
        {
            std::copy(src, src+2, dest);
            dest+=2;
            src-=2;//width-4
        }
        src+=(extendBlocks+1)<<1;//width

        src+=(extendBlocks-1)<<1;//width
        for(int i=0; i<extendBlocks; i++)
        {
            std::copy(src, src+2, dest);
            dest+=2;
            src-=2;//width-2
        }
        src+=2;//width
        std::copy(src, src+blockWidth, dest);
        dest+=blockWidth;
        src+=blockWidth;//2*width

        src-=2;//2*width-2
        for(int i=0; i<extendBlocks; i++)
        {
            std::copy(src, src+2, dest);
            dest+=2;
            src-=2;//2*width-4
        }
        src+=(extendBlocks+1)<<1;//2*width

        src-=blockWidth<<2;//-2*width
    }
    src+=blockWidth<<1;//data
#ifdef DEBUG
    if(src!=data)
        cout<<"error1 "<<data<<" "<<src<<endl;
#endif
    //copy center
    for(uint j=0; j<height; j++)
    {
        src+=(extendBlocks-1)<<1;
        for(int i=0; i<extendBlocks; i++)
        {
            std::copy(src, src+2, dest);
            dest+=2;
            src-=2;
        }
        src+=2;
#ifdef DEBUG
        if(src!=data+j*width)
            cout<<"error2 "<<data<<" "<<src<<endl;
#endif
        std::copy(src, src+blockWidth, dest);
        dest+=blockWidth;
        src+=blockWidth;

        src-=2;
        for(int i=0; i<extendBlocks; i++)
        {
            std::copy(src, src+2, dest);
            dest+=2;
            src-=2;
        }
        src+=(extendBlocks+1)<<1;
    }
#ifdef DEBUG
    if(src!=data+height*width)
        cout<<"error2 "<<data<<" "<<src<<endl;
#endif
    //copy last rows
    src-=blockWidth<<1;
    for(int j=0; j<extendBlocks; j++)
    {
        src+=(extendBlocks-1)<<1;
        for(int i=0; i<extendBlocks; i++)
        {
            std::copy(src, src+2, dest);
            dest+=2;
            src-=2;
        }
        src+=2;
        std::copy(src, src+blockWidth, dest);
        dest+=blockWidth;
        src+=blockWidth;
        src-=2;
        for(int i=0; i<extendBlocks; i++)
        {
            std::copy(src, src+2, dest);
            dest+=2;
            src-=2;
        }
        src+=(extendBlocks+1)<<1;

        src+=(extendBlocks-1)<<1;
        for(int i=0; i<extendBlocks; i++)
        {
            std::copy(src, src+2, dest);
            dest+=2;
            src-=2;
        }
        src+=2;
        std::copy(src, src+blockWidth, dest);
        dest+=blockWidth;
        src+=blockWidth;
        src-=2;
        for(int i=0; i<extendBlocks; i++)
        {
            std::copy(src, src+2, dest);
            dest+=2;
            src-=2;
        }
        src+=(extendBlocks+1)<<1;

        src-=blockWidth<<2;
    }
#ifdef DEBUG
    src+=blockWidth*(extendBlocks+1)*2;
    if(src!=data+height*width)
        cout<<"error3 "<<data<<" "<<src<<endl;
#endif
    return newArray;
}
ushort * RawWrapper::extendBayerArea(ushort* data)
{
    ushort * newArray = new ushort[(height+4)*(width+4)];
    int blockWidth=width;

    //copy first rows
    ushort * src= data;//data
    ushort * dest=newArray;

    std::copy(src, src+2, dest);
    dest+=2;
    std::copy(src, src+blockWidth, dest);
    dest+=blockWidth;
    src+=blockWidth-2;//width-2
    std::copy(src, src+2, dest);
    dest+=2;
    src+=2;//width
    std::copy(src, src+2, dest);
    dest+=2;
    std::copy(src, src+blockWidth, dest);
    dest+=blockWidth;
    src+=blockWidth-2;//2*width-2
    std::copy(src, src+2, dest);
    dest+=2;
    src+=2;//2*width

    src-=blockWidth<<1;//-2*width
#ifdef DEBUG
    if(src!=data)
        cout<<"error1 "<<data<<" "<<src<<endl;
#endif
    //copy center
    for(uint j=0; j<height; j++)
    {
        std::copy(src, src+2, dest);
        dest+=2;
#ifdef DEBUG
       if(src!=data+j*width)
            cout<<"error2 "<<data<<" "<<src<<endl;
#endif
        std::copy(src, src+blockWidth, dest);
        dest+=blockWidth;
        src+=blockWidth-2;
        std::copy(src, src+2, dest);
        dest+=2;
        src+=2;
    }
#ifdef DEBUG
    if(src!=data+height*width)
        cout<<"error2 "<<data<<" "<<src<<endl;
#endif
    //copy last rows
    src-=blockWidth<<1;
    std::copy(src, src+2, dest);
    dest+=2;
    std::copy(src, src+blockWidth, dest);
    dest+=blockWidth;
    src+=blockWidth-2;//width
    std::copy(src, src+2, dest);
    dest+=2;
    src+=2;//width
    std::copy(src, src+2, dest);
    dest+=2;
    std::copy(src, src+blockWidth, dest);
    dest+=blockWidth;
    src+=blockWidth-2;//2*width
    std::copy(src, src+2, dest);
#ifdef DEBUG
    dest+=2;
    src+=2;//2*width
    if(src!=data+height*width)
        cout<<"error3 "<<data<<" "<<src<<endl;
#endif
    return newArray;
}
