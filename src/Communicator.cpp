#include "Communicator.h"

Communicator::Communicator()
{
}
Communicator::~Communicator()
{
    if(comm!=nullptr)
        delete comm;
    if(connectionThread!=nullptr)
    {
        connectionThread->detach();
        delete connectionThread;
    }

}
void Communicator::sendFile(InputData data)
{
    static int retries=0;
    if(retries<5)
        try
        {
            if(comm!=nullptr)
            {
                comm->sendObject(data, messageType::inputData);
                cout<<"send"<<endl;
                //connectionThread=new thread(&Communicator::receive, ref(*this));
            }
        }
        catch (std::exception& e)
        {
            std::cerr <<"exception "<< e.what() << std::endl;
            retries++;
            resetConnection();
            connectionThread=new thread(&Communicator::receive, ref(*this));
            sendFile(data);
        }
    retries=0;
}
void Communicator::receive()
{
    try
    {
        for(;;)
        {
            Header m=comm->readHeader();
            switch (m.type)
            {
            case messageType::givenId:
                id=m.value;
                cout<<"givenId"<<endl;
                break;
            case messageType::outputData:
            {
                lock_guard<mutex> lock(mut);
                OutputData out = comm->readObject<OutputData>(m.value);
                outputQueue.push_back(out);
            }
            newPart.notify_one();
            cout<<"read"<<endl;
            break;
            case messageType::queryId:
                if(id==m.value)
                    comm->finishedId(id);
                cout<<"query"<<endl;
                break;
            default:
                cout<<(int)m.type<<endl;
                break;
            }
            cout<<"loop"<<endl;
        }
    }
    catch(std::exception& e)
    {
        cout<<e.what()<<endl;
        delete comm;
        comm=nullptr;
    }

}
void Communicator::init()
{
    try
    {
        resetConnection();
        connectionThread=new thread(&Communicator::receive, ref(*this));
    }
    catch(std::exception& e)
    {
        comm=nullptr;
    }
}
void Communicator::resetConnection()
{
    static int retries=0;
    if(retries<5)
        try
        {
            if(comm!=nullptr)
                delete comm;
            comm=nullptr;
            comm=new AsioCommunication();
            comm->connectAsClient(address);
        }
        catch(std::exception& e)
        {
            cout<<e.what()<<endl;
            retries++;
            resetConnection();
        }
    retries=0;

}
